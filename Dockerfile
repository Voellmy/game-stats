FROM python:3.5.2

RUN mkdir /application
WORKDIR /application

ADD . /application

RUN make setup

CMD ["python", "game_stats"]