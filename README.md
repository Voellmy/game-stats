# Game Stats

Generically compute statistics for any games.

[![build status](https://gitlab.com/Voellmy/game-stats/badges/master/build.svg)](https://gitlab.com/Voellmy/game-stats/commits/master)

[![coverage report](https://gitlab.com/Voellmy/game-stats/badges/master/coverage.svg)](https://gitlab.com/Voellmy/game-stats/commits/master)