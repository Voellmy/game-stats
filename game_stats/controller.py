import sys
import pandas as pd

from game_stats.models import Score
from game_stats.statistics.ultimate_score import UltimateScore

class Controller(object):
    def __init__(self, session):
        self.session = session

        self.ultimate_score = UltimateScore(self)


    def save_game_result(self, time, tournament_id, game_id, game_weight, result_id, player_scores):
        """
        :param time: datetime
        :param tournament_id: string
        :param game_id: string
        :param result_id: string
        :param player_scores: (string, float)
        """
        try:
            scores = []
            for score in player_scores:
                scores.append(Score(
                    time=time,
                    tournament_id=tournament_id,
                    game_id=game_id,
                    game_weight=game_weight,
                    num_players=len(player_scores),
                    player_id=score[0],
                    result_id=result_id,
                    score=score[1],
                ))
            self.session.bulk_save_objects(scores)
            self.session.commit()
        except Exception as e:
            # print to stderr and rethrow
            print(e, file=sys.stderr)
            raise Exception(e)

        return scores

    def get_scores(self, tournament_id):
        query = self.session.query(Score).filter(
            Score.tournament_id == tournament_id
            )
        return pd.read_sql(query.statement, query.session.bind)

    def get_scores_by_game(self, tournament_id, game_id):
        query = self.session.query(Score).filter(
            Score.game_id == game_id,
            Score.tournament_id == tournament_id,
            )
        return pd.read_sql(query.statement, query.session.bind)

    def get_scores_by_game_and_num_players(self, tournament_id, game_id, num_players):
        query = self.session.query(Score).filter(
            Score.game_id == game_id,
            Score.tournament_id == tournament_id,
            Score.num_players == num_players,
        )
        return pd.read_sql(query.statement, query.session.bind)

    def get_scores_by_result(self, result_id):
        query = self.session.query(Score).filter(
            Score.result_id == result_id
        )
        return pd.read_sql(query.statement, query.session.bind)

    def get_game_average(self, tournament_id, game_id, num_players):
        query = self.session.query(Score).filter(
            Score.tournament_id == tournament_id,
            Score.game_id == game_id,
            Score.num_players == num_players,
        )
        df = pd.read_sql(query.statement, query.session.bind)
        return df['score'].mean()

    def get_game_std(self, tournament_id, game_id, num_players):
        return self.get_scores_by_game_and_num_players(
            tournament_id, game_id, num_players
        )['score'].std()

    def get_ultimate_scores_of_result(self, result_id):
        return self.ultimate_score.get_ultimate_scores_of_result(result_id)
