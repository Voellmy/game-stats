class GameResult(object):

    def __init__(self):
        self.scores = []

    def add_score(self, player, score):
        self.scores.append((player, score))

    def get_scores(self):
        return list(map(lambda x: x[1], self.scores))

    def __repr__(self):
        template = """
        ------------GAME RESULT------------
        result_id: {result_id}
        tournament_id: {tournament_id}
        game_id: {game_id} (weight: {weight})
        time: {time}
        scores:
        {scores}
        -----------------------------------

        """
        scores = list(map(lambda x: '  - {}: {}'.format(x[0], x[1]), self.scores))
        scores = '\n\t\t'.join(scores)

        return template.format(
            result_id=self.result_id,
            tournament_id=self.tournament_id,
            time=self.time.strftime("%Y-%m-%d %H:%M:%S"),
            game_id=self.game_id,
            weight=self.game_weight,
            scores=scores,
        )