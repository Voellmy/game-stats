from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, DateTime, Float

Base = declarative_base()

class Score(Base):
    __tablename__ = 'scores'
    player_id = Column(String, nullable=False, primary_key=True)
    result_id = Column(String, nullable=False, primary_key=True)
    score = Column(Float, nullable=False)
    time = Column(DateTime, nullable=False)
    tournament_id = Column(String, nullable=False)
    game_id = Column(String, nullable=False)
    game_weight = Column(Float, nullable=False)
    num_players = Column(Integer, nullable=False)

    def __repr__(self):
        return "<Score(time='%s', player='%s', score='%s', tournament='%s', game='%s')>" % (
            self.time.isoformat(), self.player_id, self.score, self.tournament_id, self.game_id
        )