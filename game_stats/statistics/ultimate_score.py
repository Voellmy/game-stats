class UltimateScore:

    def __init__(self, controller):
        self.controller = controller

    def get_ultimate_scores_of_result(self, result_id):
        df = self.controller.get_scores_by_result(result_id)
        tournament_id = df['tournament_id'][0]
        game_id = df['game_id'][0]
        num_players = len(df)

        # compute mean and stdev and add it to the data frame
        mean = self.controller.get_game_average(tournament_id, game_id, num_players)
        stdev = self.controller.get_game_std(tournament_id, game_id, num_players)
        df['mean'] = mean
        df['stdev'] = stdev

        # compute ultimate score (standardized, weighted score)
        normalize = lambda row: (row['score'] - mean) / stdev
        df['ultimate_score'] = df.apply (normalize ,axis=1)

        return df