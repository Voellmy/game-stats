from unittest import TestCase as PythonTestCase

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from game_stats.models import Score

class TestCase(PythonTestCase):
    pass

class DatabaseTestCase(TestCase):

    engine = create_engine('sqlite:///:memory:')
    Session = sessionmaker(bind=engine)
    session = Session()

    # models to set up for testing
    models = [Score]

    def setUp(self):
        # set up database tables
        for model in self.models:
            model.metadata.create_all(self.engine)

    def tearDown(self):
        # remove data from db
        for model in self.models:
            self.session.query(model).delete()
