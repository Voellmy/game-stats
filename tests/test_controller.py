import statistics
from preggy import expect
from datetime import datetime

from game_stats.controller import Controller
from tests.base import DatabaseTestCase
from tests import testutils


class ControllerTestCase(DatabaseTestCase):

    def setUp(self):
        super().setUp()
        self.controller = Controller(self.session)

    def test_save_game_result(self):
        self.controller.save_game_result(
            datetime(2015, 1, 1),
            'test-tournament',
            'test-game',
            3.5,
            'test-result',
            [
                ('player1', 10),
                ('player2', 11),
                ('player3', 12),
            ]
        )

        result = self.controller.get_scores('test-tournament')

        # check the number of records
        expect(result).to_length(3)

        # check if all attributes are set correctly
        testutils.expect_attrs_to_equal(result, 'tournament_id', 'test-tournament')
        testutils.expect_attrs_to_equal(result, 'game_id', 'test-game')
        testutils.expect_attrs_to_equal(result, 'num_players', 3)
        testutils.expect_attrs_to_equal(result, 'result_id', 'test-result')

        # check players
        players = result['player_id']
        expect(players).to_include('player1')
        expect(players).to_include('player2')
        expect(players).to_include('player3')

    def test_get_scores(self):
        # create fake games
        game_result_1 = testutils.save_fake_game_result(self.controller, 1, num_players=5)
        game_result_2 = testutils.save_fake_game_result(self.controller, 2)

        result = self.controller.get_scores(game_result_1.tournament_id)

        expect(result).to_length(5)
        testutils.expect_attrs_to_equal(result, 'tournament_id', game_result_1.tournament_id)

    def test_get_by_game(self):
        # create fake games
        game_result_1 = testutils.save_fake_game_result(self.controller, 1, num_players=5)
        game_result_2 = testutils.save_fake_game_result(self.controller, 2)

        result = self.controller.get_scores_by_game(game_result_1.tournament_id, game_result_1.game_id)

        expect(result).to_length(5)
        testutils.expect_attrs_to_equal(result, 'game_id', game_result_1.game_id)


    def test_get_by_game_and_num_players(self):
        # create fake games
        game_result_1 = testutils.save_fake_game_result(self.controller, 1, num_players=5)
        game_result_2 = testutils.save_fake_game_result(self.controller, 2)

        result = self.controller.get_scores_by_game_and_num_players(
            game_result_1.tournament_id,
            game_result_1.game_id,
            5)

        expect(result).to_length(5)
        testutils.expect_attrs_to_equal(result, 'result_id', game_result_1.result_id)

    def test_get_by_result(self):
        # create fake games
        game_result_1 = testutils.save_fake_game_result(self.controller, 1, num_players=5)
        game_result_2 = testutils.save_fake_game_result(self.controller, 2)

        result = self.controller.get_scores_by_result(game_result_1.result_id)

        expect(result).to_length(5)
        testutils.expect_attrs_to_equal(result, 'result_id', game_result_1.result_id)

    def test_get_average(self):
        # create fake games
        game_result_1 = testutils.save_fake_game_result(self.controller, 1, num_players=5)
        average_score = statistics.mean(game_result_1.get_scores())

        result = self.controller.get_game_average(
            game_result_1.tournament_id,
            game_result_1.game_id,
            5
        )

        expect(result).to_equal(average_score)

    def test_get_std(self):
        # create fake game
        game_result_1 = testutils.save_fake_game_result(self.controller, 1, num_players=5)
        stdev = statistics.stdev(game_result_1.get_scores())

        result = self.controller.get_game_std(
            game_result_1.tournament_id,
            game_result_1.game_id,
            5
        )

        expect(round(result, 5)).to_equal(round(stdev, 5))
