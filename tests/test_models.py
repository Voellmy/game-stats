from preggy import expect
from datetime import datetime

from game_stats.models import Score
from tests.base import DatabaseTestCase

class ModelsTestCase(DatabaseTestCase):

    def test_can_store_score(self):
        # create a score object
        score = Score(
            time=datetime(2015, 1, 1),
            tournament_id='test-tournament',
            game_id='zhanguo',
            game_weight=3.1415,
            num_players=4,
            player_id='test-player',
            result_id='game-result-77',
            score=3.1415
        )

        # save score to the db
        self.session.add(score)
        self.session.commit()

        # retrieve object from db
        result = self.session.query(Score).get(('test-player', 'game-result-77'))

        # assertion
        expect(result).to_equal(score)
