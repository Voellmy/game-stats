from preggy import expect

from game_stats.controller import Controller
from game_stats.statistics import UltimateScore
from tests.base import DatabaseTestCase
from tests import testutils


class UltimateScoreTestCase(DatabaseTestCase):

    def setUp(self):
        super().setUp()
        self.controller = Controller(self.session)
        self.ultimate_score = UltimateScore(self.controller)

    def test_get_ultimate_scores_of_result_single(self):
        game_result = testutils.save_fake_game_result(self.controller, 1)
        result = self.ultimate_score.get_ultimate_scores_of_result(game_result.result_id)

        # if there was only one game, the sum of the ultimate
        # score is zero
        expect(round(result['ultimate_score'].sum(), 6)).to_equal(0)

    def test_get_ultimate_scores_of_result_many(self):
        # create fake games
        game_results = []

        for i in range(0, 10):
            fake_game = testutils.save_fake_game_result(
                self.controller,
                i,
                game_id='test-game',
                tournament_id='test-tournament'
            )
            game_results.append(fake_game)

        result = self.ultimate_score.get_ultimate_scores_of_result(game_results[0].result_id)

        # yes, I know there is a tiny probability that this test fails by chance.
        expect(round(result['ultimate_score'].sum(), 6)).not_to_equal(0)

    def test_get_ultimate_scores_all_equal(self):
        # create fake games
        game_results = []

        for i in range(0, 10):
            fake_game = testutils.save_fake_game_result(
                self.controller,
                i,
                game_id='test-game',
                tournament_id='test-tournament'
            )
            game_results.append(fake_game)

        same_scores = testutils.save_fake_game_result(
            self.controller,
            100,
            game_id='test-game',
            tournament_id='test-tournament',
            score=50
        )
        result = self.ultimate_score.get_ultimate_scores_of_result(same_scores.result_id)

        # should be all the same score
        expect(result['ultimate_score'].unique()).to_length(1)