from preggy import expect

from game_stats import __version__
from tests.base import TestCase


class VersionTestCase(TestCase):

    def test_has_proper_version(self):
        expect(__version__).to_equal('0.0.0')