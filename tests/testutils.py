import random
from datetime import datetime
from game_stats.models import GameResult
from preggy import expect


def save_fake_game_result(controller, key, **kwargs):
    game_result = GameResult()

    game_result.tournament_id = kwargs.get('tournament_id', 'tournament-{}'.format(key))
    game_result.game_id = kwargs.get('game_id', 'game-{}'.format(key))
    game_result.game_weight = kwargs.get('game_weight', random.random())
    game_result.result_id = kwargs.get('result_id', 'result-{}'.format(key))
    game_result.time = kwargs.get('time', datetime.now())

    if 'scores' in kwargs:
        game_result.scores = kwargs['scores']
    else:
        for i in range(0, kwargs.get('num_players', 4)):
            player_id = 'player-{}-{}'.format(key, i)
            game_result.add_score(player_id, kwargs.get('score', random.randint(0, 100)))

    controller.save_game_result(
        game_result.time,
        game_result.tournament_id,
        game_result.game_id,
        game_result.game_weight,
        game_result.result_id,
        game_result.scores
    )

    return game_result


def expect_attrs_to_equal(df, attr, expected):
    expect(df[attr].unique()).to_length(1)
    expect(df[attr].unique()).to_equal(expected)

